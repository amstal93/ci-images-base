# F-Droid CI base image

This is the base Docker image for testing F-Droid projects with
_fdroidserver_ and/or the Android SDK. This image is based on
`stretch-backports`, installs the basic components (Python, SDK, etc)
required to use _fdroidserver_ and the Android SDK.
